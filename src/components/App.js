import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import Login from './Login/Index.js';
import SignUp from './SignUp/Index.js';
import Step2 from './SignUp/Step2.js';

function App() {
  return (
    <Router>
      <div>
      <Switch>
          <Route exact path="/">
            <Login/>
          </Route>
          <Route exact path="/sign-up">
            <SignUp/>
          </Route>
          <Route exact path="/sign-up-2">
            <Step2/>
          </Route>
        </Switch>
      </div>
      </Router>
  );
}

export default App;
