import React from 'react';
import { NavLink } from "react-router-dom";

function Index() {
  return (
    <React.Fragment>

<div className="container-fluid bg-orange py-3">
   <div className="d-flex justify-content-between">
      <div>
         <img src="/assets/images/logo.png" className="img-fluid" style={{width:"150px"}}/>
      </div>
      <div>
         <NavLink to="/sign-up" activeClassName="selected" className="white ft-14 font-weight-bold pr-3">Sign Up</NavLink>
         <NavLink to="/" activeClassName="selected" className="white ft-14 font-weight-bold pr-3">Login</NavLink>
      </div>
   </div>
</div>
<div className="container">
   <form className="pt-5 mt-5 form">
      <div className="form-group">
         <label className="mr-sm-3 form-label">User</label>
         <input type="text" placeholder="Username" className="form-control"/>
      </div>
      <div className="form-group">
         <label className="mr-sm-3 form-label">Password</label>
         <input type="password" placeholder="Password" className="form-control"/>
      </div>
      <div className="form-group d-flex justify-content-end">
      <button type="submit" className="btn-submit">Login</button>
      </div>
      <div className="form-group d-flex justify-content-end">
      <NavLink to="/" className="ft-14 black">Forgot Password?</NavLink>
      </div>
   </form>
</div>        
    </React.Fragment>
  );
}

export default Index;
