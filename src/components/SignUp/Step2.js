import React from 'react';
import { NavLink } from "react-router-dom";

function Step2() {
  return (
    <React.Fragment>

<div className="container-fluid bg-orange py-3">
   <div className="d-flex justify-content-between">
      <div>
         <img src="/assets/images/logo.png" className="img-fluid" style={{width:"150px"}}/>
      </div>
      <div>
         <NavLink to="/sign-up" activeClassName="selected" className="white ft-14 font-weight-bold pr-3">Sign Up</NavLink>
         <NavLink to="/" activeClassName="selected" className="white ft-14 font-weight-bold pr-3">Login</NavLink>
      </div>
   </div>
</div>
<div className="container">
   <form className="pt-5 mt-5 form">
       <p className="ft-14">A security code has been e-mailed to your given address. Please input the code below. NB! Security code is valid for 20 minutes.</p>
      <div className="form-group">
         <label className="mr-sm-3 form-label">Security Code</label>
         <input type="text" placeholder="Security Code" className="form-control"/>
      </div>
      <div className="form-group">
         <label className="mr-sm-3 form-label">Password</label>
         <input type="password" placeholder="Password" className="form-control"/>
      </div>
      <div className="form-group">
         <label className="mr-sm-3 form-label">Password Repeat</label>
         <input type="password" placeholder="Password Repeat" className="form-control"/>
      </div>

      <div className="form-group d-flex justify-content-end">
      <button type="submit" className="btn-submit">Continue</button>
      </div>
   </form>
</div>        
    </React.Fragment>
  );
}

export default Step2;
